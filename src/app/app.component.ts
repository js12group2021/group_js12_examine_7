import { Component } from '@angular/core';
import {Detail} from "./shared/modul.detail";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  array: Detail[] = [];

  onNew(item: Detail){
    this.array.push(item);
  }
}
