import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Detail} from "../shared/modul.detail";

@Component({
  selector: 'app-cola',
  templateUrl: './cola.component.html',
  styleUrls: ['./cola.component.css']
})
export class ColaComponent {

  @Output() newCola = new EventEmitter<Detail>()
  h_Name:string = 'Cola';
  h_Cost:number = 40;

  onSubmit(){
    const Item = new Detail(this.h_Name, this.h_Cost)
    this.newCola.emit(Item);
    console.log(this.h_Name);
  }

}
