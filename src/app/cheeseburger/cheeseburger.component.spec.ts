import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheeseburgerComponent } from './cheeseburger.component';

describe('CheeseburgerComponent', () => {
  let component: CheeseburgerComponent;
  let fixture: ComponentFixture<CheeseburgerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheeseburgerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheeseburgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
