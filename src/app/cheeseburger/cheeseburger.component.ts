import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Detail} from "../shared/modul.detail";

@Component({
  selector: 'app-cheeseburger',
  templateUrl: './cheeseburger.component.html',
  styleUrls: ['./cheeseburger.component.css']
})
export class CheeseburgerComponent {

  @Output() newChB = new EventEmitter<Detail>()
  h_Name:string = 'Cheeseburger';
  h_Cost:number = 90;

  onSubmit(){
    const Item = new Detail(this.h_Name, this.h_Cost)
    this.newChB.emit(Item);
    console.log(this.h_Name);
  }

}
