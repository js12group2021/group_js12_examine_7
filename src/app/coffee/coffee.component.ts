import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Detail} from "../shared/modul.detail";

@Component({
  selector: 'app-coffee',
  templateUrl: './coffee.component.html',
  styleUrls: ['./coffee.component.css']
})
export class CoffeeComponent {
  @Output() newCofee = new EventEmitter<Detail>()
  h_Name:string = 'Cofee';
  h_Cost:number = 70;

  onSubmit(){
    const Item = new Detail(this.h_Name, this.h_Cost)
    this.newCofee.emit(Item);
    console.log(this.h_Name);
  }

}
