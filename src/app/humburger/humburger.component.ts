import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Detail} from "../shared/modul.detail";

@Component({
  selector: 'app-humburger',
  templateUrl: './humburger.component.html',
  styleUrls: ['./humburger.component.css']
})
export class HumburgerComponent {
@Output() newHam = new EventEmitter<Detail>()
 h_Name:string = 'Humburger';
 h_Cost:number = 80;

 onSubmit(){
   const Item = new Detail(this.h_Name, this.h_Cost)
   this.newHam.emit(Item);
 }

}
