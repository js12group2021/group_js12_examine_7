import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Detail} from "../shared/modul.detail";

@Component({
  selector: 'app-fries',
  templateUrl: './fries.component.html',
  styleUrls: ['./fries.component.css']
})
export class FriesComponent {

  @Output() newFries = new EventEmitter<Detail>()
  h_Name:string = 'Fries';
  h_Cost:number = 45;

  onSubmit(){
    const Item = new Detail(this.h_Name, this.h_Cost)
    this.newFries.emit(Item);
    console.log(this.h_Name);
  }

}
