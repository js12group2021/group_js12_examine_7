import {Component, Input, OnInit} from '@angular/core';
import {Detail} from "../shared/modul.detail";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent {
  @Input() item_D!: Detail;

}
