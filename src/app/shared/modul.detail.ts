export class Detail{
  constructor(
    public name:string,
    public cost: number) {}

  // getCount(){
  //   return this.count++;
  // }
  //
  getCost(cost:number){
    const sum =  cost + this.cost;
    this.cost = sum;
    return sum;
  }
}
