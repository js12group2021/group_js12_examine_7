import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Detail} from "../shared/modul.detail";

@Component({
  selector: 'app-tea',
  templateUrl: './tea.component.html',
  styleUrls: ['./tea.component.css']
})
export class TeaComponent {

  @Output() newTea = new EventEmitter<Detail>()
  h_Name:string = 'Tea';
  h_Cost:number = 50;

  onSubmit(){
    const Item = new Detail(this.h_Name, this.h_Cost)
    this.newTea.emit(Item);
    console.log(this.h_Name);
  }

}
