import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DetailsComponent } from './details/details.component';
import { HumburgerComponent } from './humburger/humburger.component';
import { FormsModule } from "@angular/forms";
import { CoffeeComponent } from './coffee/coffee.component';
import { TeaComponent } from './tea/tea.component';
import { ColaComponent } from './cola/cola.component';
import { FriesComponent } from './fries/fries.component';
import { CheeseburgerComponent } from './cheeseburger/cheeseburger.component';

@NgModule({
  declarations: [
    AppComponent,
    DetailsComponent,
    HumburgerComponent,
    CoffeeComponent,
    TeaComponent,
    ColaComponent,
    FriesComponent,
    CheeseburgerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
